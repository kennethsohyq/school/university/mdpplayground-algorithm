package robot

import map.Map
import map.MapConstants
import robot.RobotConstants.DIRECTION
import robot.RobotConstants.MOVEMENT
import utils.CommMgr
import utils.MapDescriptor

import java.util.concurrent.TimeUnit

// @formatter:off
/**
 * Represents the robot moving in the arena.
 *
 * The robot is represented by a 3 x 3 cell space as below:
 *
 * ^   ^   ^
 * SR  SR  SR
 * < SR
 * (X) [X) (X)
 * < LR (X) (X) (X) SR >
 * (X) (X) (X)
 *
 * SR = Short Range Sensor, LR = Long Range Sensor
 *
 * @author Suyash Lakhotia
 */
// @formatter:on

class Robot(row: Int, col: Int, val realBot: Boolean) {
    var robotPosRow: Int = row
        private set // center cell
    var robotPosCol: Int = col
        private set // center cell
    var robotCurDir: DIRECTION = RobotConstants.START_DIR
        private set
    private var speed: Int = RobotConstants.SPEED
    private val sensorSRFrontLeft: Sensor       // north-facing front-left SR
    private val sensorSRFrontCenter: Sensor     // north-facing front-center SR
    private val sensorSRFrontRight: Sensor      // north-facing front-right SR
    private val sensorSRLeft: Sensor            // west-facing left SR
    private val sensorSRRight: Sensor           // east-facing right SR
    private val sensorLRLeft: Sensor            // west-facing left LR
    var touchedGoal: Boolean = false
        private set

    init {
        sensorSRFrontLeft = Sensor(
            RobotConstants.SENSOR_SHORT_RANGE_L,
            RobotConstants.SENSOR_SHORT_RANGE_H,
            this.robotPosRow + 1,
            this.robotPosCol - 1,
            this.robotCurDir,
            "SRFL"
        )
        sensorSRFrontCenter = Sensor(
            RobotConstants.SENSOR_SHORT_RANGE_L,
            RobotConstants.SENSOR_SHORT_RANGE_H,
            this.robotPosRow + 1,
            this.robotPosCol,
            this.robotCurDir,
            "SRFC"
        )
        sensorSRFrontRight = Sensor(
            RobotConstants.SENSOR_SHORT_RANGE_L,
            RobotConstants.SENSOR_SHORT_RANGE_H,
            this.robotPosRow + 1,
            this.robotPosCol + 1,
            this.robotCurDir,
            "SRFR"
        )
        sensorSRLeft = Sensor(
            RobotConstants.SENSOR_SHORT_RANGE_L,
            RobotConstants.SENSOR_SHORT_RANGE_H,
            this.robotPosRow + 1,
            this.robotPosCol - 1,
            findNewDirection(MOVEMENT.LEFT),
            "SRL"
        )
        sensorSRRight = Sensor(
            RobotConstants.SENSOR_SHORT_RANGE_L,
            RobotConstants.SENSOR_SHORT_RANGE_H,
            this.robotPosRow + 1,
            this.robotPosCol + 1,
            findNewDirection(MOVEMENT.RIGHT),
            "SRR"
        )
        sensorLRLeft = Sensor(
            RobotConstants.SENSOR_LONG_RANGE_L,
            RobotConstants.SENSOR_LONG_RANGE_H,
            this.robotPosRow,
            this.robotPosCol - 1,
            findNewDirection(MOVEMENT.LEFT),
            "LRL"
        )
    }

    fun setRobotPos(row: Int, col: Int) {
        robotPosRow = row
        robotPosCol = col
    }

    fun setRobotDir(dir: DIRECTION) {
        robotCurDir = dir
    }

    fun setSpeed(speed: Int) {
        this.speed = speed
    }

    private fun updateTouchedGoal() {
        if (this.robotPosRow == MapConstants.GOAL_ROW && this.robotPosCol == MapConstants.GOAL_COL)
            this.touchedGoal = true
    }

    /**
     * Takes in a MOVEMENT and moves the robot accordingly by changing its position and direction. Sends the movement
     * if this.realBot is set.
     */
    fun move(m: MOVEMENT, sendMoveToAndroid: Boolean) {
        if (!realBot) {
            // Emulate real movement by pausing execution.
            try {
                TimeUnit.MILLISECONDS.sleep(speed.toLong())
            } catch (e: InterruptedException) {
                println("Something went wrong in Robot.move()!")
            }

        }

        when (m) {
            RobotConstants.MOVEMENT.FORWARD -> when (robotCurDir) {
                RobotConstants.DIRECTION.NORTH -> robotPosRow++
                RobotConstants.DIRECTION.EAST -> robotPosCol++
                RobotConstants.DIRECTION.SOUTH -> robotPosRow--
                RobotConstants.DIRECTION.WEST -> robotPosCol--
            }
            RobotConstants.MOVEMENT.BACKWARD -> when (robotCurDir) {
                RobotConstants.DIRECTION.NORTH -> robotPosRow--
                RobotConstants.DIRECTION.EAST -> robotPosCol--
                RobotConstants.DIRECTION.SOUTH -> robotPosRow++
                RobotConstants.DIRECTION.WEST -> robotPosCol++
            }
            RobotConstants.MOVEMENT.RIGHT, RobotConstants.MOVEMENT.LEFT -> robotCurDir = findNewDirection(m)
            RobotConstants.MOVEMENT.CALIBRATE -> {
            }
            else -> println("Error in Robot.move()!")
        }

        if (realBot)
            sendMovement(m, sendMoveToAndroid)
        else
            println("Move: " + MOVEMENT.print(m))

        updateTouchedGoal()
    }

    /**
     * Overloaded method that calls this.move(MOVEMENT m, boolean sendMoveToAndroid = true).
     */
    fun move(m: MOVEMENT) {
        this.move(m, true)
    }

    /**
     * Sends a number instead of 'F' for multiple continuous forward movements.
     */
    fun moveForwardMultiple(count: Int) {
        if (count == 1) {
            move(MOVEMENT.FORWARD)
        } else {
            val comm = CommMgr.getCommMgr()
            if (count == 10) {
                comm.sendMsg("0", CommMgr.INSTRUCTIONS)
            } else if (count < 10) {
                comm.sendMsg(Integer.toString(count), CommMgr.INSTRUCTIONS)
            }

            when (robotCurDir) {
                RobotConstants.DIRECTION.NORTH -> robotPosRow += count
                RobotConstants.DIRECTION.EAST -> robotPosCol += count
                RobotConstants.DIRECTION.SOUTH -> robotPosRow += count
                RobotConstants.DIRECTION.WEST -> robotPosCol += count
            }

            comm.sendMsg(
                this.robotPosRow.toString() + "," + this.robotPosCol + "," + DIRECTION.print(this.robotCurDir),
                CommMgr.BOT_POS
            )
        }
    }

    /**
     * Uses the CommMgr to send the next movement to the robot.
     */
    private fun sendMovement(m: MOVEMENT, sendMoveToAndroid: Boolean) {
        val comm = CommMgr.getCommMgr()
        comm.sendMsg(MOVEMENT.print(m) + "", CommMgr.INSTRUCTIONS)
        if (m !== MOVEMENT.CALIBRATE && sendMoveToAndroid) {
            comm.sendMsg(
                this.robotPosRow.toString() + "," + this.robotPosCol + "," + DIRECTION.print(this.robotCurDir),
                CommMgr.BOT_POS
            )
        }
    }

    /**
     * Sets the sensors' position and direction values according to the robot's current position and direction.
     */
    fun setSensors() {
        when (robotCurDir) {
            RobotConstants.DIRECTION.NORTH -> {
                sensorSRFrontLeft.setSensor(this.robotPosRow + 1, this.robotPosCol - 1, this.robotCurDir)
                sensorSRFrontCenter.setSensor(this.robotPosRow + 1, this.robotPosCol, this.robotCurDir)
                sensorSRFrontRight.setSensor(this.robotPosRow + 1, this.robotPosCol + 1, this.robotCurDir)
                sensorSRLeft.setSensor(this.robotPosRow + 1, this.robotPosCol - 1, findNewDirection(MOVEMENT.LEFT))
                sensorLRLeft.setSensor(this.robotPosRow, this.robotPosCol - 1, findNewDirection(MOVEMENT.LEFT))
                sensorSRRight.setSensor(this.robotPosRow + 1, this.robotPosCol + 1, findNewDirection(MOVEMENT.RIGHT))
            }
            RobotConstants.DIRECTION.EAST -> {
                sensorSRFrontLeft.setSensor(this.robotPosRow + 1, this.robotPosCol + 1, this.robotCurDir)
                sensorSRFrontCenter.setSensor(this.robotPosRow, this.robotPosCol + 1, this.robotCurDir)
                sensorSRFrontRight.setSensor(this.robotPosRow - 1, this.robotPosCol + 1, this.robotCurDir)
                sensorSRLeft.setSensor(this.robotPosRow + 1, this.robotPosCol + 1, findNewDirection(MOVEMENT.LEFT))
                sensorLRLeft.setSensor(this.robotPosRow + 1, this.robotPosCol, findNewDirection(MOVEMENT.LEFT))
                sensorSRRight.setSensor(this.robotPosRow - 1, this.robotPosCol + 1, findNewDirection(MOVEMENT.RIGHT))
            }
            RobotConstants.DIRECTION.SOUTH -> {
                sensorSRFrontLeft.setSensor(this.robotPosRow - 1, this.robotPosCol + 1, this.robotCurDir)
                sensorSRFrontCenter.setSensor(this.robotPosRow - 1, this.robotPosCol, this.robotCurDir)
                sensorSRFrontRight.setSensor(this.robotPosRow - 1, this.robotPosCol - 1, this.robotCurDir)
                sensorSRLeft.setSensor(this.robotPosRow - 1, this.robotPosCol + 1, findNewDirection(MOVEMENT.LEFT))
                sensorLRLeft.setSensor(this.robotPosRow, this.robotPosCol + 1, findNewDirection(MOVEMENT.LEFT))
                sensorSRRight.setSensor(this.robotPosRow - 1, this.robotPosCol - 1, findNewDirection(MOVEMENT.RIGHT))
            }
            RobotConstants.DIRECTION.WEST -> {
                sensorSRFrontLeft.setSensor(this.robotPosRow - 1, this.robotPosCol - 1, this.robotCurDir)
                sensorSRFrontCenter.setSensor(this.robotPosRow, this.robotPosCol - 1, this.robotCurDir)
                sensorSRFrontRight.setSensor(this.robotPosRow + 1, this.robotPosCol - 1, this.robotCurDir)
                sensorSRLeft.setSensor(this.robotPosRow - 1, this.robotPosCol - 1, findNewDirection(MOVEMENT.LEFT))
                sensorLRLeft.setSensor(this.robotPosRow - 1, this.robotPosCol, findNewDirection(MOVEMENT.LEFT))
                sensorSRRight.setSensor(this.robotPosRow + 1, this.robotPosCol - 1, findNewDirection(MOVEMENT.RIGHT))
            }
        }

    }

    /**
     * Uses the current direction of the robot and the given movement to find the new direction of the robot.
     */
    private fun findNewDirection(m: MOVEMENT): DIRECTION {
        return if (m === MOVEMENT.RIGHT) {
            DIRECTION.getNext(robotCurDir)
        } else {
            DIRECTION.getPrevious(robotCurDir)
        }
    }

    /**
     * Calls the .sense() method of all the attached sensors and stores the received values in an integer array.
     *
     * @return [sensorSRFrontLeft, sensorSRFrontCenter, sensorSRFrontRight, sensorSRLeft, sensorSRRight, sensorLRLeft]
     */
    fun sense(explorationMap: Map, realMap: Map): IntArray {
        val result = IntArray(6)

        if (!realBot) {
            result[0] = sensorSRFrontLeft.sense(explorationMap, realMap)
            result[1] = sensorSRFrontCenter.sense(explorationMap, realMap)
            result[2] = sensorSRFrontRight.sense(explorationMap, realMap)
            result[3] = sensorSRLeft.sense(explorationMap, realMap)
            result[4] = sensorSRRight.sense(explorationMap, realMap)
            result[5] = sensorLRLeft.sense(explorationMap, realMap)
        } else {
            val comm = CommMgr.getCommMgr()
            val msg = comm.recvMsg()
            val msgArr = msg!!.split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

            if (msgArr[0] == CommMgr.SENSOR_DATA) {
                result[0] =
                    Integer.parseInt(msgArr[1].split("_".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1])
                result[1] =
                    Integer.parseInt(msgArr[2].split("_".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1])
                result[2] =
                    Integer.parseInt(msgArr[3].split("_".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1])
                result[3] =
                    Integer.parseInt(msgArr[4].split("_".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1])
                result[4] =
                    Integer.parseInt(msgArr[5].split("_".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1])
                result[5] =
                    Integer.parseInt(msgArr[6].split("_".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1])
            }

            sensorSRFrontLeft.senseReal(explorationMap, result[0])
            sensorSRFrontCenter.senseReal(explorationMap, result[1])
            sensorSRFrontRight.senseReal(explorationMap, result[2])
            sensorSRLeft.senseReal(explorationMap, result[3])
            sensorSRRight.senseReal(explorationMap, result[4])
            sensorLRLeft.senseReal(explorationMap, result[5])

            val mapStrings = MapDescriptor.generateMapDescriptor(explorationMap)
            comm.sendMsg(mapStrings[0] + " " + mapStrings[1], CommMgr.MAP_STRINGS)
        }

        return result
    }
}
