package robot

/**
 * Constants used in this package.
 *
 * @author Suyash Lakhotia
 */

object RobotConstants {
    const val GOAL_ROW = 18                          // row no. of goal cell
    const val GOAL_COL = 13                          // col no. of goal cell
    const val START_ROW = 1                          // row no. of start cell
    const val START_COL = 1                          // col no. of start cell
    const val MOVE_COST = 10                         // cost of FORWARD, BACKWARD movement
    const val TURN_COST = 20                         // cost of RIGHT, LEFT movement
    const val SPEED = 100                            // delay between movements (ms)
    val START_DIR = DIRECTION.NORTH                  // start direction
    const val SENSOR_SHORT_RANGE_L = 1               // range of short range sensor (cells)
    const val SENSOR_SHORT_RANGE_H = 2               // range of short range sensor (cells)
    const val SENSOR_LONG_RANGE_L = 3                // range of long range sensor (cells)
    const val SENSOR_LONG_RANGE_H = 4                // range of long range sensor (cells)

    const val INFINITE_COST = 9999

    enum class DIRECTION {
        NORTH, EAST, SOUTH, WEST;


        companion object {

            fun getNext(curDirection: DIRECTION): DIRECTION {
                return values()[(curDirection.ordinal + 1) % values().size]
            }

            fun getPrevious(curDirection: DIRECTION): DIRECTION {
                return values()[(curDirection.ordinal + values().size - 1) % values().size]
            }

            fun print(d: DIRECTION): Char {
                return when (d) {
                    NORTH -> 'N'
                    EAST -> 'E'
                    SOUTH -> 'S'
                    WEST -> 'W'
                }
            }
        }
    }

    enum class MOVEMENT {
        FORWARD, BACKWARD, RIGHT, LEFT, CALIBRATE, ERROR;


        companion object {
            fun print(m: MOVEMENT): Char {
                return when (m) {
                    FORWARD -> 'F'
                    BACKWARD -> 'B'
                    RIGHT -> 'R'
                    LEFT -> 'L'
                    CALIBRATE -> 'C'
                    ERROR -> 'E'
                }
            }
        }
    }
}
