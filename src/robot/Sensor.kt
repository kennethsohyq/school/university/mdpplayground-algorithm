package robot

import map.Map
import robot.RobotConstants.DIRECTION

/**
 * Represents a sensor mounted on the robot.
 *
 * @author Suyash Lakhotia
 */

class Sensor(
    private val lowerRange: Int,
    private val upperRange: Int,
    private var sensorPosRow: Int,
    private var sensorPosCol: Int,
    private var sensorDir: DIRECTION?,
    private val id: String
) {

    fun setSensor(row: Int, col: Int, dir: DIRECTION) {
        this.sensorPosRow = row
        this.sensorPosCol = col
        this.sensorDir = dir
    }

    /**
     * Returns the number of cells to the nearest detected obstacle or -1 if no obstacle is detected.
     */
    fun sense(exploredMap: Map, realMap: Map): Int {
        when (sensorDir) {
            RobotConstants.DIRECTION.NORTH -> return getSensorVal(exploredMap, realMap, 1, 0)
            RobotConstants.DIRECTION.EAST -> return getSensorVal(exploredMap, realMap, 0, 1)
            RobotConstants.DIRECTION.SOUTH -> return getSensorVal(exploredMap, realMap, -1, 0)
            RobotConstants.DIRECTION.WEST -> return getSensorVal(exploredMap, realMap, 0, -1)
        }
        return -1
    }

    /**
     * Sets the appropriate obstacle cell in the map and returns the row or column value of the obstacle cell. Returns
     * -1 if no obstacle is detected.
     */
    private fun getSensorVal(exploredMap: Map, realMap: Map, rowInc: Int, colInc: Int): Int {
        // Check if starting point is valid for sensors with lowerRange > 1.
        if (lowerRange > 1) {
            for (i in 1 until this.lowerRange) {
                val row = this.sensorPosRow + rowInc * i
                val col = this.sensorPosCol + colInc * i

                if (!exploredMap.checkValidCoordinates(row, col)) return i
                if (realMap.getCell(row, col).isObstacle) return i
            }
        }

        // Check if anything is detected by the sensor and return that value.
        for (i in this.lowerRange..this.upperRange) {
            val row = this.sensorPosRow + rowInc * i
            val col = this.sensorPosCol + colInc * i

            if (!exploredMap.checkValidCoordinates(row, col)) return i

            exploredMap.getCell(row, col).isExplored = true

            if (realMap.getCell(row, col).isObstacle) {
                exploredMap.setObstacleCell(row, col, true)
                return i
            }
        }

        // Else, return -1.
        return -1
    }

    /**
     * Uses the sensor direction and given value from the actual sensor to update the map.
     */
    fun senseReal(exploredMap: Map, sensorVal: Int) {
        when (sensorDir) {
            RobotConstants.DIRECTION.NORTH -> processSensorVal(exploredMap, sensorVal, 1, 0)
            RobotConstants.DIRECTION.EAST -> processSensorVal(exploredMap, sensorVal, 0, 1)
            RobotConstants.DIRECTION.SOUTH -> processSensorVal(exploredMap, sensorVal, -1, 0)
            RobotConstants.DIRECTION.WEST -> processSensorVal(exploredMap, sensorVal, 0, -1)
        }
    }

    /**
     * Sets the correct cells to explored and/or obstacle according to the actual sensor value.
     */
    private fun processSensorVal(exploredMap: Map, sensorVal: Int, rowInc: Int, colInc: Int) {
        if (sensorVal == 0) return   // return value for LR sensor if obstacle before lowerRange

        // If above fails, check if starting point is valid for sensors with lowerRange > 1.
        for (i in 1 until this.lowerRange) {
            val row = this.sensorPosRow + rowInc * i
            val col = this.sensorPosCol + colInc * i

            if (!exploredMap.checkValidCoordinates(row, col)) return
            if (exploredMap.getCell(row, col).isObstacle) return
        }

        // Update map according to sensor's value.
        for (i in this.lowerRange..this.upperRange) {
            val row = this.sensorPosRow + rowInc * i
            val col = this.sensorPosCol + colInc * i

            if (!exploredMap.checkValidCoordinates(row, col)) continue

            exploredMap.getCell(row, col).isExplored = true

            if (sensorVal == i) {
                exploredMap.setObstacleCell(row, col, true)
                break
            }

            // Override previous obstacle value if front sensors detect no obstacle.
            if (exploredMap.getCell(row, col).isObstacle) {
                if (id == "SRFL" || id == "SRFC" || id == "SRFR") {
                    exploredMap.setObstacleCell(row, col, false)
                } else {
                    break
                }
            }
        }
    }
}
