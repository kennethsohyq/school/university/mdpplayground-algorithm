package utils

import java.io.*
import java.net.Socket
import java.net.UnknownHostException

/**
 * Communication manager to communicate with the different parts of the system via the RasPi.
 *
 * @author SuyashLakhotia
 */

class CommMgr private constructor() {

    private var writer: BufferedWriter? = null
    private var reader: BufferedReader? = null

    val isConnected: Boolean
        get() = conn!!.isConnected

    fun openConnection() {
        println("Opening connection...")

        try {
            val host = "192.168.2.1"
            val port = 8008
            conn = Socket(host, port)

            writer = BufferedWriter(OutputStreamWriter(BufferedOutputStream(conn!!.getOutputStream())))
            reader = BufferedReader(InputStreamReader(conn!!.getInputStream()))

            println("openConnection() --> " + "Connection established successfully!")

            return
        } catch (e: UnknownHostException) {
            println("openConnection() --> UnknownHostException")
        } catch (e: IOException) {
            println("openConnection() --> IOException")
        } catch (e: Exception) {
            println("openConnection() --> Exception")
            println(e.toString())
        }

        println("Failed to establish connection!")
    }

    fun closeConnection() {
        println("Closing connection...")

        try {
            reader!!.close()

            if (conn != null) {
                conn!!.close()
                conn = null
            }
            println("Connection closed!")
        } catch (e: IOException) {
            println("closeConnection() --> IOException")
        } catch (e: NullPointerException) {
            println("closeConnection() --> NullPointerException")
        } catch (e: Exception) {
            println("closeConnection() --> Exception")
            println(e.toString())
        }

    }

    fun sendMsg(msg: String?, msgType: String) {
        println("Sending a message...")

        try {
            val outputMsg: String = if (msg == null) {
                msgType + "\n"
            } else if (msgType == MAP_STRINGS || msgType == BOT_POS) {
                "$msgType $msg\n"
            } else {
                msgType + "\n" + msg + "\n"
            }

            println("Sending out message:\n$outputMsg")
            writer!!.write(outputMsg)
            writer!!.flush()
        } catch (e: IOException) {
            println("sendMsg() --> IOException")
        } catch (e: Exception) {
            println("sendMsg() --> Exception")
            println(e.toString())
        }

    }

    fun recvMsg(): String? {
        println("Receiving a message...")

        try {
            val sb = StringBuilder()
            val input = reader!!.readLine()

            if (input != null && input.isNotEmpty()) {
                sb.append(input)
                println(sb.toString())
                return sb.toString()
            }
        } catch (e: IOException) {
            println("recvMsg() --> IOException")
        } catch (e: Exception) {
            println("recvMsg() --> Exception")
            println(e.toString())
        }

        return null
    }

    companion object {

        const val EX_START = "EX_START"       // Android --> PC
        const val FP_START = "FP_START"       // Android --> PC
        const val MAP_STRINGS = "MAP"         // PC --> Android
        const val BOT_POS = "BOT_POS"         // PC --> Android
        const val BOT_START = "BOT_START"     // PC --> Arduino
        const val INSTRUCTIONS = "INSTR"      // PC --> Arduino
        const val SENSOR_DATA = "SDATA"       // Arduino --> PC

        private var commMgr: CommMgr? = null
        private var conn: Socket? = null

        fun getCommMgr(): CommMgr {
            if (commMgr == null) {
                commMgr = CommMgr()
            }
            return commMgr!!
        }
    }
}
