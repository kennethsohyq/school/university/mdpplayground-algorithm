package utils

import map.Map
import map.MapConstants
import java.io.*

/**
 * Helper methods for reading & generating map strings.
 *
 * Part 1: 1/0 represents explored state. All cells are represented.
 * Part 2: 1/0 represents obstacle state. Only explored cells are represented.
 *
 * @author Suyash Lakhotia
 */

object MapDescriptor {
    /**
     * Reads filename.txt from disk and loads it into the passed Map object. Uses a simple binary indicator to
     * identify if a cell is an obstacle.
     */
    fun loadMapFromDisk(map: Map, filename: String) {
        try {
            val inputStream = FileInputStream("maps/$filename.txt")
            val buf = BufferedReader(InputStreamReader(inputStream))

            map.resetMap() // Reset playing field first

            var line: String? = buf.readLine()
            val sb = StringBuilder()
            while (line != null) {
                sb.append(line)
                line = buf.readLine()
            }

            val bin = sb.toString()
            var binPtr = 0
            for (row in MapConstants.MAP_ROWS - 1 downTo 0) {
                for (col in 0 until MapConstants.MAP_COLS) {
                    if (bin[binPtr] == '1') map.setObstacleCell(row, col, true)
                    binPtr++
                }
            }

            map.setAllExplored()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    /**
     * Gets a list of all maps in the maps folder
     */
    fun getListOfAllMaps(): ArrayList<String> {
        val folder = File("maps/")
        val files = folder.listFiles()
        val result = ArrayList<String>()
        files.forEach { s ->
            run {
                result.add(s.name.trimEnd('.','t','x','t'))
            }
        }
        return result
    }

    /**
     * Helper method to convert a binary string to a hex string.
     */
    private fun binToHex(bin: String): String {
        val dec = Integer.parseInt(bin, 2)

        return Integer.toHexString(dec)
    }

    /**
     * Generates Part 1 & Part 2 map descriptor strings from the passed Map object.
     */
    fun generateMapDescriptor(map: Map): Array<String?> {
        val ret = arrayOfNulls<String>(2)

        val part1 = StringBuilder()
        val part1Bin = StringBuilder()
        part1Bin.append("11")
        for (r in 0 until MapConstants.MAP_ROWS) {
            for (c in 0 until MapConstants.MAP_COLS) {
                if (map.getCell(r, c).isExplored)
                    part1Bin.append("1")
                else
                    part1Bin.append("0")

                if (part1Bin.length == 4) {
                    part1.append(binToHex(part1Bin.toString()))
                    part1Bin.setLength(0)
                }
            }
        }
        part1Bin.append("11")
        part1.append(binToHex(part1Bin.toString()))
        println("P1: $part1")
        ret[0] = part1.toString()

        val part2 = StringBuilder()
        val part2Bin = StringBuilder()
        for (r in 0 until MapConstants.MAP_ROWS) {
            for (c in 0 until MapConstants.MAP_COLS) {
                if (map.getCell(r, c).isExplored) {
                    if (map.getCell(r, c).isObstacle)
                        part2Bin.append("1")
                    else
                        part2Bin.append("0")

                    if (part2Bin.length == 4) {
                        part2.append(binToHex(part2Bin.toString()))
                        part2Bin.setLength(0)
                    }
                }
            }
        }
        if (part2Bin.isNotEmpty()) part2.append(binToHex(part2Bin.toString()))
        println("P2: $part2")
        ret[1] = part2.toString()

        return ret
    }
}
