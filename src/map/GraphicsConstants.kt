package map

import java.awt.Color

/**
 * Constants used in the Map class for rendering the arena in the simulator.
 *
 * @author Suyash Lakhotia
 */

internal object GraphicsConstants {
    const val CELL_LINE_WEIGHT = 2

    val C_START = Color.BLUE!!
    val C_GOAL = Color.GREEN!!
    val C_UNEXPLORED = Color.LIGHT_GRAY!!
    val C_FREE = Color.WHITE!!
    val C_OBSTACLE = Color.BLACK!!

    val C_ROBOT = Color.RED!!
    val C_ROBOT_DIR = Color.WHITE!!

    const val ROBOT_W = 70
    const val ROBOT_H = 70

    const val ROBOT_X_OFFSET = 10
    const val ROBOT_Y_OFFSET = 20

    const val ROBOT_DIR_W = 10
    const val ROBOT_DIR_H = 10

    const val CELL_SIZE = 30

    const val MAP_H = 600
    const val MAP_X_OFFSET = 120
}
