package map

/**
 * Represents each cell in the map grid.
 *
 * @author Suyash Lakhotia
 */

class Cell(val row: Int, val col: Int) {
    var isObstacle = false
    var isVirtualWall = false
        set(`val`) {
            if (`val`) {
                field = true
            } else {
                if (row != 0 && row != MapConstants.MAP_ROWS - 1 && col != 0 && col != MapConstants.MAP_COLS - 1) {
                    field = false
                }
            }
        }
    var isExplored = false
}
