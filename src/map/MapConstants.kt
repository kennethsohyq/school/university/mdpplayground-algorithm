package map

/**
 * Constants used in the Map class.
 *
 * @author Suyash Lakhotia
 */

object MapConstants {
    const val MAP_SIZE = 300     // total num of cells
    const val MAP_ROWS = 20      // total num of rows
    const val MAP_COLS = 15      // total num of cols
    const val GOAL_ROW = 18      // row no. of goal cell
    const val GOAL_COL = 13      // col no. of goal cell
}
