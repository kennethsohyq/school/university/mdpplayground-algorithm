package map

import robot.Robot
import robot.RobotConstants
import java.awt.Color
import java.awt.Graphics
import javax.swing.JPanel

/**
 * Represents the entire map grid for the arena.
 *
 * @author Suyash Lakhotia
 */

class Map
/**
 * Initialises a Map object with a grid of Cell objects.
 */
    (private val bot: Robot) : JPanel() {
    private val grid: Array<Array<Cell?>> = Array(MapConstants.MAP_ROWS) { arrayOfNulls<Cell>(MapConstants.MAP_COLS) }

    init {
        for (row in grid.indices) {
            for (col in 0 until grid[0].size) {
                grid[row][col] = Cell(row, col)

                // Set the virtual walls of the arena
                if (row == 0 || col == 0 || row == MapConstants.MAP_ROWS - 1 || col == MapConstants.MAP_COLS - 1) {
                    grid[row][col]!!.isVirtualWall = true
                }
            }
        }
    }

    /**
     * Returns true if the row and column values are valid.
     */
    fun checkValidCoordinates(row: Int, col: Int): Boolean {
        return row >= 0 && col >= 0 && row < MapConstants.MAP_ROWS && col < MapConstants.MAP_COLS
    }

    /**
     * Resets the map
     */
    fun resetMap() {
        for (row in grid.indices) {
            for (col in 0 until grid[row].size) {
                setObstacleCell(row, col, false)
            }
        }
    }

    /**
     * Returns true if the row and column values are in the start zone.
     */
    private fun inStartZone(row: Int, col: Int): Boolean {
        return row in 0..2 && col >= 0 && col <= 2
    }

    /**
     * Returns true if the row and column values are in the goal zone.
     */
    private fun inGoalZone(row: Int, col: Int): Boolean {
        return row <= MapConstants.GOAL_ROW + 1 && row >= MapConstants.GOAL_ROW - 1 && col <= MapConstants.GOAL_COL + 1 && col >= MapConstants.GOAL_COL - 1
    }

    /**
     * Returns a particular cell in the grid.
     */
    fun getCell(row: Int, col: Int): Cell {
        return grid[row][col]!!
    }

    /**
     * Returns true if a cell is an obstacle.
     */
    fun isObstacleCell(row: Int, col: Int): Boolean {
        return grid[row][col]!!.isObstacle
    }

    /**
     * Returns true if a cell is a virtual wall.
     */
    fun isVirtualWallCell(row: Int, col: Int): Boolean {
        return grid[row][col]!!.isVirtualWall
    }

    /**
     * Sets all cells in the grid to an explored state.
     */
    fun setAllExplored() {
        for (row in grid.indices) {
            for (col in 0 until grid[0].size) {
                grid[row][col]!!.isExplored = true
            }
        }
    }

    /**
     * Sets all cells in the grid to an unexplored state except for the START & GOAL zone.
     */
    fun setAllUnexplored() {
        for (row in grid.indices) {
            for (col in 0 until grid[0].size) {
                grid[row][col]!!.isExplored = inStartZone(row, col) || inGoalZone(row, col)
            }
        }
    }

    /**
     * Sets a cell as an obstacle and the surrounding cells as virtual walls or resets the cell and surrounding
     * virtual walls.
     */
    fun setObstacleCell(row: Int, col: Int, obstacle: Boolean) {
        if (obstacle && (inStartZone(row, col) || inGoalZone(row, col)))
            return

        grid[row][col]!!.isObstacle = obstacle

        if (row >= 1) {
            grid[row - 1][col]!!.isVirtualWall = obstacle            // bottom cell

            if (col < MapConstants.MAP_COLS - 1) {
                grid[row - 1][col + 1]!!.isVirtualWall = obstacle    // bottom-right cell
            }

            if (col >= 1) {
                grid[row - 1][col - 1]!!.isVirtualWall = obstacle    // bottom-left cell
            }
        }

        if (row < MapConstants.MAP_ROWS - 1) {
            grid[row + 1][col]!!.isVirtualWall = obstacle            // top cell

            if (col < MapConstants.MAP_COLS - 1) {
                grid[row + 1][col + 1]!!.isVirtualWall = obstacle    // top-right cell
            }

            if (col >= 1) {
                grid[row + 1][col - 1]!!.isVirtualWall = obstacle    // top-left cell
            }
        }

        if (col >= 1) {
            grid[row][col - 1]!!.isVirtualWall = obstacle            // left cell
        }

        if (col < MapConstants.MAP_COLS - 1) {
            grid[row][col + 1]!!.isVirtualWall = obstacle            // right cell
        }
    }

    /**
     * Returns true if the given cell is out of bounds or an obstacle.
     */
    fun isObstacleOrWall(row: Int, col: Int): Boolean {
        return !checkValidCoordinates(row, col) || getCell(row, col).isObstacle
    }

    /**
     * Overrides JComponent's paintComponent() method. It creates a two-dimensional array of DisplayCell objects
     * to store the current map state. Then, it paints square cells for the grid with the appropriate colors as
     * well as the robot on-screen.
     */
    public override fun paintComponent(g: Graphics) {
        // Create a two-dimensional array of DisplayCell objects for rendering.
        super.paintComponent(g)
        val mapCells = Array<Array<DisplayCell?>>(MapConstants.MAP_ROWS) { arrayOfNulls(MapConstants.MAP_COLS) }
        for (mapRow in 0 until MapConstants.MAP_ROWS) {
            for (mapCol in 0 until MapConstants.MAP_COLS) {
                mapCells[mapRow][mapCol] = DisplayCell(
                    mapCol * GraphicsConstants.CELL_SIZE,
                    mapRow * GraphicsConstants.CELL_SIZE,
                    GraphicsConstants.CELL_SIZE
                )
            }
        }

        // Paint the cells with the appropriate colors.
        for (mapRow in 0 until MapConstants.MAP_ROWS) {
            for (mapCol in 0 until MapConstants.MAP_COLS) {
                val cellColor: Color = if (inStartZone(mapRow, mapCol))
                    GraphicsConstants.C_START
                else if (inGoalZone(mapRow, mapCol))
                    GraphicsConstants.C_GOAL
                else {
                    if (!grid[mapRow][mapCol]!!.isExplored)
                        GraphicsConstants.C_UNEXPLORED
                    else if (grid[mapRow][mapCol]!!.isObstacle)
                        GraphicsConstants.C_OBSTACLE
                    else
                        GraphicsConstants.C_FREE
                }

                g.color = cellColor
                g.fillRect(
                    mapCells[mapRow][mapCol]!!.cellX + GraphicsConstants.MAP_X_OFFSET,
                    mapCells[mapRow][mapCol]!!.cellY,
                    mapCells[mapRow][mapCol]!!.cellSize,
                    mapCells[mapRow][mapCol]!!.cellSize
                )

            }
        }

        // Paint the robot on-screen.
        g.color = GraphicsConstants.C_ROBOT
        val r = bot.robotPosRow
        val c = bot.robotPosCol
        g.fillOval(
            (c - 1) * GraphicsConstants.CELL_SIZE + GraphicsConstants.ROBOT_X_OFFSET + GraphicsConstants.MAP_X_OFFSET,
            GraphicsConstants.MAP_H - (r * GraphicsConstants.CELL_SIZE + GraphicsConstants.ROBOT_Y_OFFSET),
            GraphicsConstants.ROBOT_W,
            GraphicsConstants.ROBOT_H
        )

        // Paint the robot's direction indicator on-screen.
        g.color = GraphicsConstants.C_ROBOT_DIR
        when (bot.robotCurDir) {
            RobotConstants.DIRECTION.NORTH -> g.fillOval(
                c * GraphicsConstants.CELL_SIZE + 10 + GraphicsConstants.MAP_X_OFFSET,
                GraphicsConstants.MAP_H - r * GraphicsConstants.CELL_SIZE - 15,
                GraphicsConstants.ROBOT_DIR_W,
                GraphicsConstants.ROBOT_DIR_H
            )
            RobotConstants.DIRECTION.EAST -> g.fillOval(
                c * GraphicsConstants.CELL_SIZE + 35 + GraphicsConstants.MAP_X_OFFSET,
                GraphicsConstants.MAP_H - r * GraphicsConstants.CELL_SIZE + 10,
                GraphicsConstants.ROBOT_DIR_W,
                GraphicsConstants.ROBOT_DIR_H
            )
            RobotConstants.DIRECTION.SOUTH -> g.fillOval(
                c * GraphicsConstants.CELL_SIZE + 10 + GraphicsConstants.MAP_X_OFFSET,
                GraphicsConstants.MAP_H - r * GraphicsConstants.CELL_SIZE + 35,
                GraphicsConstants.ROBOT_DIR_W,
                GraphicsConstants.ROBOT_DIR_H
            )
            RobotConstants.DIRECTION.WEST -> g.fillOval(
                c * GraphicsConstants.CELL_SIZE - 15 + GraphicsConstants.MAP_X_OFFSET,
                GraphicsConstants.MAP_H - r * GraphicsConstants.CELL_SIZE + 10,
                GraphicsConstants.ROBOT_DIR_W,
                GraphicsConstants.ROBOT_DIR_H
            )
        }
    }

    private inner class DisplayCell(borderX: Int, borderY: Int, borderSize: Int) {
        val cellX: Int = borderX + GraphicsConstants.CELL_LINE_WEIGHT
        val cellY: Int = GraphicsConstants.MAP_H - (borderY - GraphicsConstants.CELL_LINE_WEIGHT)
        val cellSize: Int = borderSize - GraphicsConstants.CELL_LINE_WEIGHT * 2
    }
}
