package algorithms

import map.Cell
import map.Map
import map.MapConstants
import robot.Robot
import robot.RobotConstants
import robot.RobotConstants.DIRECTION
import robot.RobotConstants.MOVEMENT
import java.util.*

// @formatter:off
/**
 * Fastest path algorithm for the robot. Uses a version of the A* algorithm.
 *
 * g(n) = Real Cost from START to n
 * h(n) = Heuristic Cost from n to GOAL
 *
 * @author Suyash Lakhotia
 */
// @formatter:on

class FastestPathAlgo {
    private var toVisit: ArrayList<Cell> = ArrayList()                          // array of Cells to be visited
    private var visited: ArrayList<Cell> = ArrayList()                          // array of visited Cells
    private var parents: HashMap<Cell?, Cell?> = HashMap()                      // HashMap of Child --> Parent
    private var current: Cell? = null                                         // current Cell
    private var neighbors: Array<Cell?> = arrayOfNulls(4)               // array of neighbors of current Cell
    private var curDir: DIRECTION? = null                                   // current direction of robot
    private var gCosts: Array<DoubleArray>? = null                        // array of real cost from START to [row][col] i.e. g(n)
    private var bot: Robot? = null
    private var exploredMap: Map? = null
    private val realMap: Map?
    private var loopCount: Int = 0
    private var explorationMode: Boolean = false

    constructor(exploredMap: Map, bot: Robot) {
        this.realMap = null
        initObject(exploredMap, bot)
    }

    constructor(exploredMap: Map, bot: Robot, realMap: Map) {
        this.realMap = realMap
        this.explorationMode = true
        initObject(exploredMap, bot)
    }

    /**
     * Initialise the FastestPathAlgo object.
     */
    private fun initObject(map: Map, bot: Robot) {
        this.bot = bot
        this.exploredMap = map
        this.current = map.getCell(bot.robotPosRow, bot.robotPosCol)
        this.curDir = bot.robotCurDir
        this.gCosts = Array(MapConstants.MAP_ROWS) { DoubleArray(MapConstants.MAP_COLS) }

        // Initialise gCosts array
        for (i in 0 until MapConstants.MAP_ROWS) {
            for (j in 0 until MapConstants.MAP_COLS) {
                val cell = map.getCell(i, j)
                if (!canBeVisited(cell)) {
                    gCosts!![i][j] = RobotConstants.INFINITE_COST.toDouble()
                } else {
                    gCosts!![i][j] = 0.0
                }
            }
        }
        toVisit.add(current!!)

        // Initialise starting point
        gCosts!![bot.robotPosRow][bot.robotPosCol] = 0.0
        this.loopCount = 0
    }

    /**
     * Returns true if the cell can be visited.
     */
    private fun canBeVisited(c: Cell): Boolean {
        return c.isExplored && !c.isObstacle && !c.isVirtualWall
    }

    /**
     * Returns the Cell inside toVisit with the minimum g(n) + h(n).
     */
    private fun minimumCostCell(goalRow: Int, getCol: Int): Cell? {
        val size = toVisit.size
        var minCost = RobotConstants.INFINITE_COST.toDouble()
        var result: Cell? = null

        for (i in size - 1 downTo 0) {
            val gCost = gCosts!![toVisit[i].row][toVisit[i].col]
            val cost = gCost + costH(toVisit[i], goalRow, getCol)
            if (cost < minCost) {
                minCost = cost
                result = toVisit[i]
            }
        }

        return result
    }

    /**
     * Returns the heuristic cost i.e. h(n) from a given Cell to a given [goalRow, goalCol] in the maze.
     */
    private fun costH(b: Cell, goalRow: Int, goalCol: Int): Double {
        // Heuristic: The no. of moves will be equal to the difference in the row and column values.
        val movementCost =
            ((Math.abs(goalCol - b.col) + Math.abs(goalRow - b.row)) * RobotConstants.MOVE_COST).toDouble()

        if (movementCost == 0.0) return 0.0

        // Heuristic: If b is not in the same row or column, one turn will be needed.
        var turnCost = 0.0
        if (goalCol - b.col != 0 || goalRow - b.row != 0) {
            turnCost = RobotConstants.TURN_COST.toDouble()
        }

        return movementCost + turnCost
    }

    /**
     * Returns the target direction of the bot from [botR, botC] to target Cell.
     */
    private fun getTargetDir(botR: Int, botC: Int, botDir: DIRECTION?, target: Cell): DIRECTION? {
        return when {
            botC - target.col > 0 -> DIRECTION.WEST
            target.col - botC > 0 -> DIRECTION.EAST
            else -> when {
                botR - target.row > 0 -> DIRECTION.SOUTH
                target.row - botR > 0 -> DIRECTION.NORTH
                else -> botDir
            }
        }
    }

    /**
     * Get the actual turning cost from one DIRECTION to another.
     */
    private fun getTurnCost(a: DIRECTION, b: DIRECTION): Double {
        var numOfTurn = Math.abs(a.ordinal - b.ordinal)
        if (numOfTurn > 2) {
            numOfTurn %= 2
        }
        return (numOfTurn * RobotConstants.TURN_COST).toDouble()
    }

    /**
     * Calculate the actual cost of moving from Cell a to Cell b (assuming both are neighbors).
     */
    private fun costG(a: Cell, b: Cell, aDir: DIRECTION?): Double {
        val moveCost = RobotConstants.MOVE_COST.toDouble() // one movement to neighbor

        val turnCost: Double
        val targetDir = getTargetDir(a.row, a.col, aDir, b)
        turnCost = getTurnCost(aDir!!, targetDir!!)

        return moveCost + turnCost
    }

    /**
     * Find the fastest path from the robot's current position to [goalRow, goalCol].
     */
    fun runFastestPath(goalRow: Int, goalCol: Int): String? {
        println("Calculating fastest path from (" + current!!.row + ", " + current!!.col + ") to goal (" + goalRow + ", " + goalCol + ")...")

        val path: Stack<Cell>
        do {
            loopCount++

            // Get cell with minimum cost from toVisit and assign it to current.
            current = minimumCostCell(goalRow, goalCol)

            // Point the robot in the direction of current from the previous cell.
            if (parents.containsKey(current)) {
                curDir = getTargetDir(parents[current]!!.row, parents[current]!!.col, curDir, current!!)
            }

            visited.add(current!!)       // add current to visited
            toVisit.remove(current!!)    // remove current from toVisit

            if (visited.contains(exploredMap!!.getCell(goalRow, goalCol))) {
                println("Goal visited. Path found!")
                path = getPath(goalRow, goalCol)
                printFastestPath(path)
                return executePath(path, goalRow, goalCol)
            }

            // Setup neighbors of current cell. [Top, Bottom, Left, Right].
            if (exploredMap!!.checkValidCoordinates(current!!.row + 1, current!!.col)) {
                neighbors[0] = exploredMap!!.getCell(current!!.row + 1, current!!.col)
                if (!canBeVisited(neighbors[0]!!)) {
                    neighbors[0] = null
                }
            }
            if (exploredMap!!.checkValidCoordinates(current!!.row - 1, current!!.col)) {
                neighbors[1] = exploredMap!!.getCell(current!!.row - 1, current!!.col)
                if (!canBeVisited(neighbors[1]!!)) {
                    neighbors[1] = null
                }
            }
            if (exploredMap!!.checkValidCoordinates(current!!.row, current!!.col - 1)) {
                neighbors[2] = exploredMap!!.getCell(current!!.row, current!!.col - 1)
                if (!canBeVisited(neighbors[2]!!)) {
                    neighbors[2] = null
                }
            }
            if (exploredMap!!.checkValidCoordinates(current!!.row, current!!.col + 1)) {
                neighbors[3] = exploredMap!!.getCell(current!!.row, current!!.col + 1)
                if (!canBeVisited(neighbors[3]!!)) {
                    neighbors[3] = null
                }
            }

            // Iterate through neighbors and update the g(n) values of each.
            for (i in 0..3) {
                if (neighbors[i] != null) {
                    if (visited.contains(neighbors[i])) {
                        continue
                    }

                    if (!toVisit.contains(neighbors[i])) {
                        parents[neighbors[i]] = current
                        gCosts!![neighbors[i]!!.row][neighbors[i]!!.col] =
                            gCosts!![current!!.row][current!!.col] + costG(current!!, neighbors[i]!!, curDir)
                        toVisit.add(neighbors[i]!!)
                    } else {
                        val currentGScore = gCosts!![neighbors[i]!!.row][neighbors[i]!!.col]
                        val newGScore =
                            gCosts!![current!!.row][current!!.col] + costG(current!!, neighbors[i]!!, curDir)
                        if (newGScore < currentGScore) {
                            gCosts!![neighbors[i]!!.row][neighbors[i]!!.col] = newGScore
                            parents[neighbors[i]] = current
                        }
                    }
                }
            }
        } while (toVisit.isNotEmpty())

        println("Path not found!")
        return null
    }

    /**
     * Generates path in reverse using the parents HashMap.
     */
    private fun getPath(goalRow: Int, goalCol: Int): Stack<Cell> {
        val actualPath = Stack<Cell>()
        var temp: Cell? = exploredMap!!.getCell(goalRow, goalCol)

        while (true) {
            actualPath.push(temp)
            temp = parents[temp]
            if (temp == null) {
                break
            }
        }

        return actualPath
    }

    /**
     * Executes the fastest path and returns a StringBuilder object with the path steps.
     */
    private fun executePath(path: Stack<Cell>, goalRow: Int, goalCol: Int): String {
        val outputString = StringBuilder()

        var temp = path.pop()
        var targetDir: DIRECTION?

        val movements = ArrayList<MOVEMENT>()

        val tempBot = Robot(1, 1, false)
        tempBot.setSpeed(0)
        while (tempBot.robotPosRow != goalRow || tempBot.robotPosCol != goalCol) {
            if (tempBot.robotPosRow == temp.row && tempBot.robotPosCol == temp.col) {
                temp = path.pop()
            }

            targetDir = getTargetDir(tempBot.robotPosRow, tempBot.robotPosCol, tempBot.robotCurDir, temp)

            val m: MOVEMENT = if (tempBot.robotCurDir != targetDir) {
                getTargetMove(tempBot.robotCurDir, targetDir)
            } else {
                MOVEMENT.FORWARD
            }

            println("Movement " + MOVEMENT.print(m) + " from (" + tempBot.robotPosRow + ", " + tempBot.robotPosCol + ") to (" + temp.row + ", " + temp.col + ")")

            tempBot.move(m)
            movements.add(m)
            outputString.append(MOVEMENT.print(m))
        }

        if (!bot!!.realBot || explorationMode) {
            for (x in movements) {
                if (x == MOVEMENT.FORWARD) {
                    if (!canMoveForward()) {
                        println("Early termination of fastest path execution.")
                        return "T"
                    }
                }

                bot!!.move(x)
                this.exploredMap!!.repaint()

                // During exploration, use sensor data to update exploredMap.
                if (explorationMode) {
                    bot!!.setSensors()
                    bot!!.sense(this.exploredMap!!, this.realMap!!)
                    this.exploredMap!!.repaint()
                }
            }
        } else {
            var fCount = 0
            for (x in movements) {
                if (x == MOVEMENT.FORWARD) {
                    fCount++
                    if (fCount == 10) {
                        bot!!.moveForwardMultiple(fCount)
                        fCount = 0
                        exploredMap!!.repaint()
                    }
                } else if (x == MOVEMENT.RIGHT || x == MOVEMENT.LEFT) {
                    if (fCount > 0) {
                        bot!!.moveForwardMultiple(fCount)
                        fCount = 0
                        exploredMap!!.repaint()
                    }

                    bot!!.move(x)
                    exploredMap!!.repaint()
                }
            }

            if (fCount > 0) {
                bot!!.moveForwardMultiple(fCount)
                exploredMap!!.repaint()
            }
        }

        println("\nMovements: $outputString")
        return outputString.toString()
    }

    /**
     * Returns true if the robot can move forward one cell with the current heading.
     */
    private fun canMoveForward(): Boolean {
        val row = bot!!.robotPosRow
        val col = bot!!.robotPosCol

        when (bot!!.robotCurDir) {
            RobotConstants.DIRECTION.NORTH -> if (!exploredMap!!.isObstacleCell(
                    row + 2,
                    col - 1
                ) && !exploredMap!!.isObstacleCell(row + 2, col) && !exploredMap!!.isObstacleCell(row + 2, col + 1)
            ) {
                return true
            }
            RobotConstants.DIRECTION.EAST -> if (!exploredMap!!.isObstacleCell(
                    row + 1,
                    col + 2
                ) && !exploredMap!!.isObstacleCell(row, col + 2) && !exploredMap!!.isObstacleCell(row - 1, col + 2)
            ) {
                return true
            }
            RobotConstants.DIRECTION.SOUTH -> if (!exploredMap!!.isObstacleCell(
                    row - 2,
                    col - 1
                ) && !exploredMap!!.isObstacleCell(row - 2, col) && !exploredMap!!.isObstacleCell(row - 2, col + 1)
            ) {
                return true
            }
            RobotConstants.DIRECTION.WEST -> if (!exploredMap!!.isObstacleCell(
                    row + 1,
                    col - 2
                ) && !exploredMap!!.isObstacleCell(row, col - 2) && !exploredMap!!.isObstacleCell(row - 1, col - 2)
            ) {
                return true
            }
        }

        return false
    }

    /**
     * Returns the movement to execute to get from one direction to another.
     */
    private fun getTargetMove(a: DIRECTION, b: DIRECTION?): MOVEMENT {
        when (a) {
            RobotConstants.DIRECTION.NORTH -> when (b) {
                RobotConstants.DIRECTION.NORTH -> return MOVEMENT.ERROR
                RobotConstants.DIRECTION.SOUTH -> return MOVEMENT.LEFT
                RobotConstants.DIRECTION.WEST -> return MOVEMENT.LEFT
                RobotConstants.DIRECTION.EAST -> return MOVEMENT.RIGHT
            }
            RobotConstants.DIRECTION.SOUTH -> when (b) {
                RobotConstants.DIRECTION.NORTH -> return MOVEMENT.LEFT
                RobotConstants.DIRECTION.SOUTH -> return MOVEMENT.ERROR
                RobotConstants.DIRECTION.WEST -> return MOVEMENT.RIGHT
                RobotConstants.DIRECTION.EAST -> return MOVEMENT.LEFT
            }
            RobotConstants.DIRECTION.WEST -> when (b) {
                RobotConstants.DIRECTION.NORTH -> return MOVEMENT.RIGHT
                RobotConstants.DIRECTION.SOUTH -> return MOVEMENT.LEFT
                RobotConstants.DIRECTION.WEST -> return MOVEMENT.ERROR
                RobotConstants.DIRECTION.EAST -> return MOVEMENT.LEFT
            }
            RobotConstants.DIRECTION.EAST -> when (b) {
                RobotConstants.DIRECTION.NORTH -> return MOVEMENT.LEFT
                RobotConstants.DIRECTION.SOUTH -> return MOVEMENT.RIGHT
                RobotConstants.DIRECTION.WEST -> return MOVEMENT.LEFT
                RobotConstants.DIRECTION.EAST -> return MOVEMENT.ERROR
            }
        }
        return MOVEMENT.ERROR
    }

    /**
     * Prints the fastest path from the Stack object.
     */
    private fun printFastestPath(path: Stack<Cell>) {
        println("\nLooped $loopCount times.")
        println("The number of steps is: " + (path.size - 1) + "\n")

        @Suppress("UNCHECKED_CAST")
        val pathForPrint = path.clone() as Stack<Cell>
        var temp: Cell
        println("Path:")
        while (!pathForPrint.isEmpty()) {
            temp = pathForPrint.pop()
            if (!pathForPrint.isEmpty())
                print("(" + temp.row + ", " + temp.col + ") --> ")
            else
                print("(" + temp.row + ", " + temp.col + ")")
        }

        println("\n")
    }

    /**
     * Prints all the current g(n) values for the cells.
     */
    fun printGCosts() {
        for (i in 0 until MapConstants.MAP_ROWS) {
            for (j in 0 until MapConstants.MAP_COLS) {
                print(gCosts!![MapConstants.MAP_ROWS - 1 - i][j])
                print(";")
            }
            println("\n")
        }
    }
}
