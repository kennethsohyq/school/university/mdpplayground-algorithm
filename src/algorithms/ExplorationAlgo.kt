package algorithms

import map.Map
import map.MapConstants
import robot.Robot
import robot.RobotConstants
import robot.RobotConstants.DIRECTION
import robot.RobotConstants.MOVEMENT
import utils.CommMgr

/**
 * Exploration algorithm for the robot.
 *
 * @author Priyanshu Singh
 * @author Suyash Lakhotia
 */

class ExplorationAlgo(
    private val exploredMap: Map,
    private val realMap: Map,
    private val bot: Robot,
    private val coverageLimit: Int,
    private val timeLimit: Int
) {
    private var areaExplored: Int = 0
    private var startTime: Long = 0
    private var endTime: Long = 0
    private var lastCalibrate: Int = 0
    private var calibrationMode: Boolean = false

    /**
     * Returns a possible direction for robot calibration or null, otherwise.
     */
    private val calibrationDirection: DIRECTION?
        get() {
            val origDir = bot.robotCurDir
            var dirToCheck: DIRECTION

            dirToCheck = DIRECTION.getNext(origDir) // right turn
            if (canCalibrateOnTheSpot(dirToCheck)) return dirToCheck

            dirToCheck = DIRECTION.getPrevious(origDir) // left turn
            if (canCalibrateOnTheSpot(dirToCheck)) return dirToCheck

            dirToCheck = DIRECTION.getPrevious(dirToCheck)// u turn
            return if (canCalibrateOnTheSpot(dirToCheck)) dirToCheck else null

        }

    /**
     * Main method that is called to start the exploration.
     */
    fun runExploration() {
        if (bot.realBot) {
            println("Starting calibration...")

            CommMgr.getCommMgr().recvMsg()
            if (bot.realBot) {
                bot.move(MOVEMENT.LEFT, false)
                CommMgr.getCommMgr().recvMsg()
                bot.move(MOVEMENT.CALIBRATE, false)
                CommMgr.getCommMgr().recvMsg()
                bot.move(MOVEMENT.LEFT, false)
                CommMgr.getCommMgr().recvMsg()
                bot.move(MOVEMENT.CALIBRATE, false)
                CommMgr.getCommMgr().recvMsg()
                bot.move(MOVEMENT.RIGHT, false)
                CommMgr.getCommMgr().recvMsg()
                bot.move(MOVEMENT.CALIBRATE, false)
                CommMgr.getCommMgr().recvMsg()
                bot.move(MOVEMENT.RIGHT, false)
            }

            while (true) {
                println("Waiting for EX_START...")
                val msg = CommMgr.getCommMgr().recvMsg()
                val msgArr = msg!!.split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                if (msgArr[0] == CommMgr.EX_START) break
            }
        }

        println("Starting exploration...")

        startTime = System.currentTimeMillis()
        endTime = startTime + timeLimit * 1000

        if (bot.realBot) {
            CommMgr.getCommMgr().sendMsg(null, CommMgr.BOT_START)
        }
        senseAndRepaint()

        areaExplored = calculateAreaExplored()
        println("Explored Area: $areaExplored")

        explorationLoop(bot.robotPosRow, bot.robotPosCol)
    }

    /**
     * Loops through robot movements until one (or more) of the following conditions is met:
     * 1. Robot is back at (r, c)
     * 2. areaExplored > coverageLimit
     * 3. System.currentTimeMillis() > endTime
     */
    private fun explorationLoop(r: Int, c: Int) {
        do {
            nextMove()

            areaExplored = calculateAreaExplored()
            println("Area explored: $areaExplored")

            if (bot.robotPosRow == r && bot.robotPosCol == c) {
                if (areaExplored >= 100) {
                    break
                }
            }
        } while (areaExplored <= coverageLimit && System.currentTimeMillis() <= endTime)

        goHome()
    }

    /**
     * Determines the next move for the robot and executes it accordingly.
     */
    private fun nextMove() {
        if (lookRight()) {
            moveBot(MOVEMENT.RIGHT)
            if (lookForward()) moveBot(MOVEMENT.FORWARD)
        } else if (lookForward()) {
            moveBot(MOVEMENT.FORWARD)
        } else if (lookLeft()) {
            moveBot(MOVEMENT.LEFT)
            if (lookForward()) moveBot(MOVEMENT.FORWARD)
        } else {
            moveBot(MOVEMENT.RIGHT)
            moveBot(MOVEMENT.RIGHT)
        }
    }

    /**
     * Returns true if the right side of the robot is free to move into.
     */
    private fun lookRight(): Boolean {
        return when (bot.robotCurDir) {
            RobotConstants.DIRECTION.NORTH -> eastFree()
            RobotConstants.DIRECTION.EAST -> southFree()
            RobotConstants.DIRECTION.SOUTH -> westFree()
            RobotConstants.DIRECTION.WEST -> northFree()
        }
    }

    /**
     * Returns true if the robot is free to move forward.
     */
    private fun lookForward(): Boolean {
        return when (bot.robotCurDir) {
            RobotConstants.DIRECTION.NORTH -> northFree()
            RobotConstants.DIRECTION.EAST -> eastFree()
            RobotConstants.DIRECTION.SOUTH -> southFree()
            RobotConstants.DIRECTION.WEST -> westFree()
        }
    }

    /**
     * * Returns true if the left side of the robot is free to move into.
     */
    private fun lookLeft(): Boolean {
        return when (bot.robotCurDir) {
            RobotConstants.DIRECTION.NORTH -> westFree()
            RobotConstants.DIRECTION.EAST -> northFree()
            RobotConstants.DIRECTION.SOUTH -> eastFree()
            RobotConstants.DIRECTION.WEST -> southFree()
        }
    }

    /**
     * Returns true if the robot can move to the north cell.
     */
    private fun northFree(): Boolean {
        val botRow = bot.robotPosRow
        val botCol = bot.robotPosCol
        return isExploredNotObstacle(botRow + 1, botCol - 1) && isExploredAndFree(
            botRow + 1,
            botCol
        ) && isExploredNotObstacle(botRow + 1, botCol + 1)
    }

    /**
     * Returns true if the robot can move to the east cell.
     */
    private fun eastFree(): Boolean {
        val botRow = bot.robotPosRow
        val botCol = bot.robotPosCol
        return isExploredNotObstacle(botRow - 1, botCol + 1) && isExploredAndFree(
            botRow,
            botCol + 1
        ) && isExploredNotObstacle(botRow + 1, botCol + 1)
    }

    /**
     * Returns true if the robot can move to the south cell.
     */
    private fun southFree(): Boolean {
        val botRow = bot.robotPosRow
        val botCol = bot.robotPosCol
        return isExploredNotObstacle(botRow - 1, botCol - 1) && isExploredAndFree(
            botRow - 1,
            botCol
        ) && isExploredNotObstacle(botRow - 1, botCol + 1)
    }

    /**
     * Returns true if the robot can move to the west cell.
     */
    private fun westFree(): Boolean {
        val botRow = bot.robotPosRow
        val botCol = bot.robotPosCol
        return isExploredNotObstacle(botRow - 1, botCol - 1) && isExploredAndFree(
            botRow,
            botCol - 1
        ) && isExploredNotObstacle(botRow + 1, botCol - 1)
    }

    /**
     * Returns the robot to START after exploration and points the bot northwards.
     */
    private fun goHome() {
        if (!bot.touchedGoal && coverageLimit == 300 && timeLimit == 3600) {
            val goToGoal = FastestPathAlgo(exploredMap, bot, realMap)
            goToGoal.runFastestPath(RobotConstants.GOAL_ROW, RobotConstants.GOAL_COL)
        }

        val returnToStart = FastestPathAlgo(exploredMap, bot, realMap)
        returnToStart.runFastestPath(RobotConstants.START_ROW, RobotConstants.START_COL)

        println("Exploration complete!")
        areaExplored = calculateAreaExplored()
        System.out.printf("%.2f%% Coverage", areaExplored / 300.0 * 100.0)
        println(", $areaExplored Cells")
        println(((System.currentTimeMillis() - startTime) / 1000).toString() + " Seconds")

        if (bot.realBot) {
            turnBotDirection(DIRECTION.WEST)
            moveBot(MOVEMENT.CALIBRATE)
            turnBotDirection(DIRECTION.SOUTH)
            moveBot(MOVEMENT.CALIBRATE)
            turnBotDirection(DIRECTION.WEST)
            moveBot(MOVEMENT.CALIBRATE)
        }
        turnBotDirection(DIRECTION.NORTH)
    }

    /**
     * Returns true for cells that are explored and not obstacles.
     */
    private fun isExploredNotObstacle(r: Int, c: Int): Boolean {
        if (exploredMap.checkValidCoordinates(r, c)) {
            val tmp = exploredMap.getCell(r, c)
            return tmp.isExplored && !tmp.isObstacle
        }
        return false
    }

    /**
     * Returns true for cells that are explored, not virtual walls and not obstacles.
     */
    private fun isExploredAndFree(r: Int, c: Int): Boolean {
        if (exploredMap.checkValidCoordinates(r, c)) {
            val b = exploredMap.getCell(r, c)
            return b.isExplored && !b.isVirtualWall && !b.isObstacle
        }
        return false
    }

    /**
     * Returns the number of cells explored in the grid.
     */
    private fun calculateAreaExplored(): Int {
        var result = 0
        for (r in 0 until MapConstants.MAP_ROWS) {
            for (c in 0 until MapConstants.MAP_COLS) {
                if (exploredMap.getCell(r, c).isExplored) {
                    result++
                }
            }
        }
        return result
    }

    /**
     * Moves the bot, repaints the map and calls senseAndRepaint().
     */
    private fun moveBot(m: MOVEMENT) {
        bot.move(m)
        exploredMap.repaint()
        if (m != MOVEMENT.CALIBRATE) {
            senseAndRepaint()
        } else {
            val commMgr = CommMgr.getCommMgr()
            commMgr.recvMsg()
        }

        if (bot.realBot && !calibrationMode) {
            calibrationMode = true

            if (canCalibrateOnTheSpot(bot.robotCurDir)) {
                lastCalibrate = 0
                moveBot(MOVEMENT.CALIBRATE)
            } else {
                lastCalibrate++
                if (lastCalibrate >= 5) {
                    val targetDir = calibrationDirection
                    if (targetDir != null) {
                        lastCalibrate = 0
                        calibrateBot(targetDir)
                    }
                }
            }

            calibrationMode = false
        }
    }

    /**
     * Sets the bot's sensors, processes the sensor data and repaints the map.
     */
    private fun senseAndRepaint() {
        bot.setSensors()
        bot.sense(exploredMap, realMap)
        exploredMap.repaint()
    }

    /**
     * Checks if the robot can calibrate at its current position given a direction.
     */
    private fun canCalibrateOnTheSpot(botDir: DIRECTION): Boolean {
        val row = bot.robotPosRow
        val col = bot.robotPosCol

        when (botDir) {
            RobotConstants.DIRECTION.NORTH -> return exploredMap.isObstacleOrWall(
                row + 2,
                col - 1
            ) && exploredMap.isObstacleOrWall(row + 2, col) && exploredMap.isObstacleOrWall(row + 2, col + 1)
            RobotConstants.DIRECTION.EAST -> return exploredMap.isObstacleOrWall(
                row + 1,
                col + 2
            ) && exploredMap.isObstacleOrWall(row, col + 2) && exploredMap.isObstacleOrWall(row - 1, col + 2)
            RobotConstants.DIRECTION.SOUTH -> return exploredMap.isObstacleOrWall(
                row - 2,
                col - 1
            ) && exploredMap.isObstacleOrWall(row - 2, col) && exploredMap.isObstacleOrWall(row - 2, col + 1)
            RobotConstants.DIRECTION.WEST -> return exploredMap.isObstacleOrWall(
                row + 1,
                col - 2
            ) && exploredMap.isObstacleOrWall(row, col - 2) && exploredMap.isObstacleOrWall(row - 1, col - 2)
        }
    }

    /**
     * Turns the bot in the needed direction and sends the CALIBRATE movement. Once calibrated, the bot is turned back
     * to its original direction.
     */
    private fun calibrateBot(targetDir: DIRECTION) {
        val origDir = bot.robotCurDir

        turnBotDirection(targetDir)
        moveBot(MOVEMENT.CALIBRATE)
        turnBotDirection(origDir)
    }

    /**
     * Turns the robot to the required direction.
     */
    private fun turnBotDirection(targetDir: DIRECTION) {
        var numOfTurn = Math.abs(bot.robotCurDir.ordinal - targetDir.ordinal)
        if (numOfTurn > 2) numOfTurn %= 2

        if (numOfTurn == 1) {
            if (DIRECTION.getNext(bot.robotCurDir) == targetDir) {
                moveBot(MOVEMENT.RIGHT)
            } else {
                moveBot(MOVEMENT.LEFT)
            }
        } else if (numOfTurn == 2) {
            moveBot(MOVEMENT.RIGHT)
            moveBot(MOVEMENT.RIGHT)
        }
    }
}
