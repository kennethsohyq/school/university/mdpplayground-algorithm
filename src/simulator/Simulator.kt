package simulator

import algorithms.ExplorationAlgo
import algorithms.FastestPathAlgo
import map.Map
import map.MapConstants
import robot.Robot
import robot.RobotConstants
import utils.CommMgr
import utils.MapDescriptor
import java.awt.*
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.*

/**
 * Simulator for robot navigation in virtual arena.
 *
 * @author Suyash Lakhotia
 */

object Simulator {
    private var appFrame: JFrame? = null         // application JFrame

    private var mapCards: JPanel? = null         // JPanel for map views
    private var buttons: JPanel? = null          // JPanel for buttons

    private var bot: Robot? = null

    private var realMap: Map? = null              // real map
    private var exploredMap: Map? = null          // exploration map

    private var timeLimit = 3600            // time limit
    private var coverageLimit = 300         // coverage limit

    private val comm = CommMgr.getCommMgr()
    private var realRun = false // Modify to true if actual run

    private var loadedFont: Font = Font.createFont(Font.TRUETYPE_FONT, Simulator.javaClass.getResourceAsStream("/Roboto-Regular.ttf"))

    /**
     * Initialises the different maps and displays the application.
     */
    @JvmStatic
    fun main(args: Array<String>) {
        if (realRun) comm.openConnection()

        bot = Robot(RobotConstants.START_ROW, RobotConstants.START_COL, realRun)

        if (!realRun) {
            realMap = Map(bot!!)
            realMap!!.setAllUnexplored()
        }

        exploredMap = Map(bot!!)
        exploredMap!!.setAllUnexplored()

        displayEverything()
    }

    /**
     * Initialises the different parts of the application.
     */
    private fun displayEverything() {
        // Initialise main frame for display
        appFrame = JFrame()
        appFrame!!.title = "MDP Group 2 Simulator"
        appFrame!!.size = Dimension(690, 700)
        appFrame!!.isResizable = false

        // Center the main frame in the middle of the screen
        val dim = Toolkit.getDefaultToolkit().screenSize
        appFrame!!.setLocation(
            dim.width / 2 - appFrame!!.size.width / 2,
            dim.height / 2 - appFrame!!.size.height / 2
        )

        // Create the CardLayout for storing the different maps
        mapCards = JPanel(CardLayout())

        // Create the JPanel for the buttons
        buttons = JPanel()

        // Add mapCards & buttons to the main frame's content pane
        val contentPane = appFrame!!.contentPane
        contentPane.add(mapCards!!, BorderLayout.CENTER)
        contentPane.add(buttons!!, BorderLayout.PAGE_END)

        // Initialize the main map view
        initMainLayout()

        // Initialize the buttons
        initButtonsLayout()

        // Display the application
        appFrame!!.isVisible = true
        appFrame!!.defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
    }

    /**
     * Initialises the main map view by adding the different maps as cards in the CardLayout. Displays realMap
     * by default.
     */
    private fun initMainLayout() {
        if (!realRun) {
            mapCards!!.add(realMap!!, "REAL_MAP")
        }
        mapCards!!.add(exploredMap!!, "EXPLORATION")

        val cl = mapCards!!.layout as CardLayout
        if (!realRun) {
            cl.show(mapCards!!, "REAL_MAP")
        } else {
            cl.show(mapCards!!, "EXPLORATION")
        }
    }

    /**
     * Initialises the JPanel for the buttons.
     */
    private fun initButtonsLayout() {
        buttons!!.layout = GridLayout()
        addButtons()
    }

    /**
     * Helper method to set particular properties for all the JButtons.
     */
    private fun formatButton(btn: JButton) {
        btn.font = loadedFont.deriveFont(13f).deriveFont(Font.BOLD)
        btn.isFocusPainted = false
    }

    /**
     * Initialises and adds the five main buttons. Also creates the relevant classes (for multithreading) and JDialogs
     * (for user input) for the different functions of the buttons.
     */
    private fun addButtons() {
        if (!realRun) {
            // Load Map Button
            val btnLoadMap = JButton("Load Map")
            formatButton(btnLoadMap)
            btnLoadMap.addMouseListener(object : MouseAdapter() {
                override fun mousePressed(e: MouseEvent?) {
                    val loadMapDialog = JDialog(appFrame, "Load Map", true)
                    loadMapDialog.setSize(400, 80)
                    loadMapDialog.layout = FlowLayout()

                    val mapsList = MapDescriptor.getListOfAllMaps().toTypedArray()
                    val loadMapSelection = JComboBox(mapsList)

                    val loadMapButton = JButton("Load")
                    formatButton(loadMapButton)

                    loadMapButton.addMouseListener(object : MouseAdapter() {
                        override fun mousePressed(e: MouseEvent?) {
                            loadMapDialog.isVisible = false
                            MapDescriptor.loadMapFromDisk(realMap!!, loadMapSelection.selectedItem as String)
                            val cl = mapCards!!.layout as CardLayout
                            cl.show(mapCards!!, "REAL_MAP")
                            realMap!!.repaint()
                        }
                    })

                    loadMapDialog.add(JLabel("Select a map: "))
                    loadMapDialog.add(loadMapSelection)
                    loadMapDialog.add(loadMapButton)
                    loadMapDialog.isVisible = true
                }
            })
            buttons!!.add(btnLoadMap)
        }

        // FastestPath Class for Multithreading
        class FastestPath : SwingWorker<Int, String>() {
            @Throws(Exception::class)
            override fun doInBackground(): Int? {
                bot!!.setRobotPos(RobotConstants.START_ROW, RobotConstants.START_COL)
                exploredMap!!.repaint()

                if (realRun) {
                    while (true) {
                        println("Waiting for FP_START...")
                        val msg = comm.recvMsg()
                        if (msg == CommMgr.FP_START) break
                    }
                }

                val fastestPath = FastestPathAlgo(exploredMap!!, bot!!)

                fastestPath.runFastestPath(RobotConstants.GOAL_ROW, RobotConstants.GOAL_COL)

                return 222
            }
        }

        // Exploration Class for Multithreading
        class Exploration : SwingWorker<Int, String>() {
            @Throws(Exception::class)
            override fun doInBackground(): Int? {
                val row: Int = RobotConstants.START_ROW
                val col: Int = RobotConstants.START_COL

                bot!!.setRobotPos(row, col)
                exploredMap!!.repaint()

                val exploration = ExplorationAlgo(exploredMap!!, realMap!!, bot!!, coverageLimit, timeLimit)

                if (realRun) {
                    CommMgr.getCommMgr().sendMsg(null, CommMgr.BOT_START)
                }

                exploration.runExploration()
                MapDescriptor.generateMapDescriptor(exploredMap!!)

                if (realRun) {
                    FastestPath().execute()
                }

                return 111
            }
        }

        // Exploration Button
        val btnExploration = JButton("Exploration")
        formatButton(btnExploration)
        btnExploration.addMouseListener(object : MouseAdapter() {
            override fun mousePressed(e: MouseEvent?) {
                val cl = mapCards!!.layout as CardLayout
                cl.show(mapCards!!, "EXPLORATION")
                Exploration().execute()
            }
        })
        buttons!!.add(btnExploration)

        // Fastest Path Button
        val btnFastestPath = JButton("Fastest Path")
        formatButton(btnFastestPath)
        btnFastestPath.addMouseListener(object : MouseAdapter() {
            override fun mousePressed(e: MouseEvent?) {
                val cl = mapCards!!.layout as CardLayout
                cl.show(mapCards!!, "EXPLORATION")
                FastestPath().execute()
            }
        })
        buttons!!.add(btnFastestPath)


        // TimeExploration Class for Multithreading
        class TimeExploration : SwingWorker<Int, String>() {
            @Throws(Exception::class)
            override fun doInBackground(): Int? {
                bot!!.setRobotPos(RobotConstants.START_ROW, RobotConstants.START_COL)
                exploredMap!!.repaint()

                val timeExplo = ExplorationAlgo(exploredMap!!, realMap!!, bot!!, coverageLimit, timeLimit)
                timeExplo.runExploration()

                MapDescriptor.generateMapDescriptor(exploredMap!!)

                return 333
            }
        }

        // Time-limited Exploration Button
        val btnTimeExploration = JButton("Time-Limited")
        formatButton(btnTimeExploration)
        btnTimeExploration.addMouseListener(object : MouseAdapter() {
            override fun mousePressed(e: MouseEvent?) {
                val timeExploDialog = JDialog(appFrame, "Time-Limited Exploration", true)
                timeExploDialog.setSize(400, 80)
                timeExploDialog.layout = FlowLayout()
                val timeTF = JTextField(5)
                val timeSaveButton = JButton("Run")
                formatButton(timeSaveButton)

                timeSaveButton.addMouseListener(object : MouseAdapter() {
                    override fun mousePressed(e: MouseEvent?) {
                        timeExploDialog.isVisible = false
                        val time = timeTF.text
                        val timeArr = time.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                        timeLimit = Integer.parseInt(timeArr[0]) * 60 + Integer.parseInt(timeArr[1])
                        val cl = mapCards!!.layout as CardLayout
                        cl.show(mapCards!!, "EXPLORATION")
                        TimeExploration().execute()
                    }
                })

                timeExploDialog.add(JLabel("Time Limit (in MM:SS): "))
                timeExploDialog.add(timeTF)
                timeExploDialog.add(timeSaveButton)
                timeExploDialog.isVisible = true
            }
        })
        buttons!!.add(btnTimeExploration)


        // CoverageExploration Class for Multithreading
        class CoverageExploration : SwingWorker<Int, String>() {
            @Throws(Exception::class)
            override fun doInBackground(): Int? {
                bot!!.setRobotPos(RobotConstants.START_ROW, RobotConstants.START_COL)
                exploredMap!!.repaint()

                val coverageExplo = ExplorationAlgo(exploredMap!!, realMap!!, bot!!, coverageLimit, timeLimit)
                coverageExplo.runExploration()

                MapDescriptor.generateMapDescriptor(exploredMap!!)

                return 444
            }
        }

        // Coverage-limited Exploration Button
        val btnCoverageExploration = JButton("Coverage-Limited")
        formatButton(btnCoverageExploration)
        btnCoverageExploration.addMouseListener(object : MouseAdapter() {
            override fun mousePressed(e: MouseEvent?) {
                val coverageExploDialog = JDialog(appFrame, "Coverage-Limited Exploration", true)
                coverageExploDialog.setSize(400, 80)
                coverageExploDialog.layout = FlowLayout()
                val coverageTF = JTextField(5)
                val coverageSaveButton = JButton("Run")
                formatButton(coverageSaveButton)

                coverageSaveButton.addMouseListener(object : MouseAdapter() {
                    override fun mousePressed(e: MouseEvent?) {
                        coverageExploDialog.isVisible = false
                        coverageLimit = (Integer.parseInt(coverageTF.text) * MapConstants.MAP_SIZE / 100.0).toInt()
                        CoverageExploration().execute()
                        val cl = mapCards!!.layout as CardLayout
                        cl.show(mapCards!!, "EXPLORATION")
                    }
                })

                coverageExploDialog.add(JLabel("Coverage Limit (% of maze): "))
                coverageExploDialog.add(coverageTF)
                coverageExploDialog.add(coverageSaveButton)
                coverageExploDialog.isVisible = true
            }
        })
        buttons!!.add(btnCoverageExploration)
    }
}
